ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG DAPK
ARG APKVER

ENV dqsver 1.2.1

# hadolint ignore=DL3018
# RUN sed -i -e 's/v[[:digit:]]\..*\//edge\//g' /etc/apk/repositories \
RUN apk add --no-cache --upgrade drill rspamd rspamd-controller rspamd-fuzzy rspamd-proxy stunnel \
&& mkdir /run/rspamd && chown rspamd:rspamd /run/rspamd \
&& mkdir /run/stunnel && chown stunnel:stunnel /run/stunnel

# if DAPK is not set bake file defaults it to alpine-base
RUN echo "DAPK is: $DAPK" \
&& apk list -q $DAPK | awk '{print $1}'> /etc/apkvers \
&& cat /etc/apkvers

WORKDIR /tmp
RUN wget -q https://github.com/spamhaus/rspamd-dqs/archive/v${dqsver}.tar.gz \
&& tar -xzf v${dqsver}.tar.gz \
&& mv rspamd-dqs-${dqsver}/3.x /etc/rspamd/rspamd-dqs \
&& rm -Rf ./*

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh container-scripts/health-nc.sh entrypoint.sh update_*.sh ./

WORKDIR /etc/rspamd/local.d
COPY local.conf ./

WORKDIR /etc/rspamd/local.d/maps.orig
COPY --chown=rspamd:rspamd maps/* ./

COPY stunnel.conf /etc/stunnel/stunnel.conf

CMD [ "entrypoint.sh" ]
VOLUME /var/lib/rspamd /etc/rspamd/override.d /etc/rspamd/local.d/maps.d
EXPOSE 11332 11334

HEALTHCHECK --start-period=60s CMD wget -q -O - 127.0.0.1:11334/healthy | grep -q "success"
